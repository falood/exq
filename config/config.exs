use Mix.Config

config :exq,
    adapter: :mysql,
    host: "127.0.0.1",
    port: 3306,
    database: "lazysql_test",
    username: "root",
    password: "root"
